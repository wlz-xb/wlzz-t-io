package com.websocket.constant;

/**
 * redis常量
 * Created by yhn on 2017/9/5.
 */
public interface RedisConstant {
    String TOKEN_PREFIX = "token_%s";
    String LOGIN_SUCCESS_TOKEN = "login_token_%s";
    String PHONE_PREFIX = "phone_%s";
    String LOGIN_PREFIX = "loginCode_%s";
    Integer EXPIRE =1296000;  //1个月
    Integer CODE_EXPIRE=300;
    Integer PHONE_EXPIRE=60;
}
