package com.websocket.dto;

import com.websocket.dto.WechatDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yhn on 2018/6/8.
 */
@Data
public class DialogueList implements Serializable {
    //不区分谁与谁,一聊天就会生成两条记录即两个对话(不包含聊天记录)
    private String _id;
    private Integer userid1;   //聊天人
    private Integer userid2;  //聊天人2
    private Integer flag;     // 删除标志
    private WechatDTO data;  //最新的一条聊天数据
    private Date createTime;//创建时间
}
