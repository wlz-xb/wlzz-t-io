package com.websocket.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Data
public class WechatDTO implements Serializable{
    private String _id;
    private String toUserid;
    private String userid;
    private String content;
    private Date create_time;
    private int flag;
    @Override
    public String toString() {
        return "WechatDTO{" +
                "_id='" + _id + '\'' +
                ", toUserid='" + toUserid + '\'' +
                ", userid='" + userid + '\'' +
                ", content='" + content + '\'' +
                ", create_time=" + create_time +
                ", flag=" + flag +
                '}';
    }
}
