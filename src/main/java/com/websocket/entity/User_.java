package com.websocket.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author YHN
 * @version 2018-03-27
 */
@Entity
@Data  //lombok表达式注解(省略setter和getter)
public class User_ { //个人注册
    @Id
    @GeneratedValue
    private Integer userId;
    private String phone;    //手机号码
    private String password; //密码
    private String salt;  //用户盐值-->如果需要以后加密使用。
    private String weChatOpenid;  //微信授权标识--->即绑定
    private String qqOpenid;  //qq授权登陆标识--->即绑定
    private Boolean isDisplay; //是否公开
    private Boolean examine; //审核是否通过
    private Date createTime;  //创建时间
    private Date upDateTime;  //更新时间
}
