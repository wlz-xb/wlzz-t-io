package com.websocket.repository.mongo;

import com.websocket.dto.DialogueList;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * Created by yhn on 2018/6/8.
 */
public interface DialogueListRepository extends MongoRepository<DialogueList,String>{
    List<DialogueList> findByUserid1AndFlagOrderByCreateTimeDesc(Integer userid1,Integer flag);   //查询所有的会话列表
    DialogueList findByUserid1AndUserid2AndFlag(Integer userid1,Integer userid2,Integer flag);  //查询所在对话
}
