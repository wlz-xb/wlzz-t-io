<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/wlzzim/bui/css/bui.css"/>
    <script type="text/javascript" src="/wlzzim/contacts/js/jquery-1.8.3.min.js"></script>
    <script src="/wlzzim/layer_mobile/layer.js"></script>
    <#--<script>-->
    <#--//loading带文字-->
    <#--layer.open({-->
        <#--type: 2,-->
        <#--content: '加载中',-->
        <#--shadeClose: false-->
    <#--});-->
<#--</script>-->
    <style>
        .span2{
            font-size: 16px;
            color: black;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap
        }
        @font-face {
            font-family: 'iconfont';
            src: url('/wlzzim/bui/font/iconfont.eot'); /* IE9*/
            src: url('/wlzzim/bui/font/iconfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
            url('/wlzzim/bui/font/iconfont.woff') format('woff'), /* chrome、firefox */
            url('/wlzzim/bui/font/iconfont.ttf') format('truetype'), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
            url('/wlzzim/bui/font/iconfont.svg#iconfont') format('svg'); /* iOS 4.1- */
        }
        .iconfont{
            font-family:"iconfont" !important;
            font-size:30px;font-style:normal;
            -webkit-font-smoothing: antialiased;
            color: white;
            -webkit-text-stroke-width: 0.2px;
            -moz-osx-font-smoothing: grayscale;}
        .icon{
            display: block;
            margin-top: 7px;
            margin-left: 7px;
        }
        p{
            margin-left: 15%;
        }
        .bestNews{
            font-size: 10px;
            color: #4e4a49;
        }
    </style>
    <script src="/wlzzim/bui/js/zepto.js"></script>
    <script src="/wlzzim/bui/js/bui.js"></script>

</head>
<script>
    function reloadPage(){
        //停止刷新页面
        // location.reload();
        tiows.reconn();
        document.getElementById("iframeDiv").contentWindow.getAllFriend();
    }
    function getHeadImg(imgUrl){
        if(imgUrl=="/wlzz/images/logo.jpg"){
            return '${domainName}'+imgUrl;
        }else{
            return imgUrl;
        }
    }
    var timeout = undefined;
    function longTouch(id) {
        timeout = setTimeout(function() {
            //底部对话框
            layer.open({
                content: '您是否要删除该好友的对话'
                ,btn: [ '取消','删除']
                ,skin: 'footer'
                ,yes: function(index){  //是取反哦
                    layer.close(index);
                },no: function (index) {
                    var token = androidFun.getToken();
                    $.post("/wlzzim/index/deleteDialog",{token:token,userid2:id},function(data) {
                            if("success"==data){
                                var list_ = document.getElementById("list_"+id);
                                list_.parentNode.removeChild(list_);
                                //提示
                                layer.open({
                                    content: '成功删除该会话'
                                    ,skin: 'msg'
                                    ,time: 2 //2秒后自动关闭
                                });

                            }else{
                                //提示
                                layer.open({
                                    content: data
                                    ,skin: 'msg'
                                    ,time: 2 //2秒后自动关闭
                                });
                            }
                    });
                }
            });
        }, 1300);
    }
    function nolongTouch(){
        clearTimeout(timeout);
    }
    // $(function(){
    //    var token = androidFun.getUserId();
    //    androidFun.showAndroidToast(token);
    // });


    function parentDemo(id){
        // console.log(id);
        // bui.load({
        //     url: "/wlzzim/index/chat?toUserid="+id+"&userId="+userid,
        // });
        var userid = androidFun.getUserId();
        var token = androidFun.getToken();
        $.post("http://39.108.97.134:8886/wlzz/getInfo/useridGetForInfo",{token:token,userid:id},function(data) {
            var obj = eval('(' + data + ')');
            if(obj.code==0){
                androidFun.newPage("与"+obj.data.nickName+"聊天","http://39.108.97.134:8801/wlzzim/index/chat?toUserid="+id+"&userId="+userid);
            }else{
                androidFun.showAndroidToast("该用户异常无法与他聊天");
            }
        });

    }
    function parentToTab(id){
        var tab = bui.slide({
            id:"#tabNews",
            menu:"#tabNav",
            children:".bui-tab-main > ul",
            scroll: true,
        });
        tab.to(id);
    }
    window.onload=function(){
            parentToTab(1);
    }
    function searchInput() {
        androidFun.newPage("搜索或添加好友","http://39.108.97.134:8801/wlzzim/index/search");
        // bui.load({
        //     url: "/wlzzim/index/search",
        // })
    }
</script>
<body>
<!-- 侧滑栏的父层, -->
<input hidden id="server_ip" value="${server_ip}">
<input hidden id="server_port" value="${server_port}">
<div id="sidebarWrap" class="bui-sidebar-wrap">
    <div class="bui-page">
        <header class="bui-bar">
            <div class="bui-bar-left" id="menu"></div>
            <div class="bui-bar-main">
            <ul id="tabNav" class="bui-nav">
                <li class="bui-btn">
                    好友列表
                </li>
                <li class="bui-btn">
                    会话列表
                </li>
            </ul>
            </div>
            <div class="bui-bar-right">

           </div>
        </header>
        <main>
            <div id="tabNews" class="bui-tab">
                <div class="bui-tab-main">
                    <ul>
                        <ol>
                            <div id="hammerDemo" style="width: 100%;height: 100%">
                            <#--<iframe src="/wlzzim/index/contacts" width="100%" height="100%"  frameborder=0></iframe>-->

                                <object data="/wlzzim/index/contacts" type="text/x-scriptlet" width="100%" style="float: right" height="100%" align="top" marginWidth=0 marginHeight=0 id="iframeDiv">
                                </object>
                            </div>
                        </ol>
                        <ol>
                            <!-- 搜索条控件结构 -->
                            <div class="bui-searchbar bui-box searchbar">
                                <div class="span1">
                                    <div class="bui-input">
                                        <i class="icon-search"></i>
                                        <input type="search" value="" onclick="searchInput()"  placeholder="搜索或添加好友"/>
                                    </div>
                                </div>
                            </div>
                            <ul id="listview" class="bui-listview ">
                            </ul>
                        </ol>
                    </ul>
                </div>
            </div>
        </main>
        <footer></footer>
    </div>
</div>
</body>

<!-- 组件js，和业务不相关的 -->
<script src="/wlzzim/tio/js/dialogue/tiows.js"></script>
<!-- 和业务相关的js，业务需要修改 -->
<script src="/wlzzim/tio/js/dialogue/DemoHandler.js"></script>
<script src="/wlzzim/tio/js/dialogue/demo.js"></script>

</html>