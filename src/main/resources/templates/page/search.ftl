<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="icon" href="https://static.jianshukeji.com/hcode/images/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/wlzzim/bui/css/bui.css"/>
    <script src="/wlzzim/layer/mobile/layer.js"></script>
    <style>
        @font-face {font-family: 'iconfont';
            src: url('/wlzzim/bui/font/iconfont.eot'); /* IE9*/
            src: url('/wlzzim/bui/font/iconfont.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
            url('/wlzzim/bui/font/iconfont.woff') format('woff'), /* chrome、firefox */
            url('/wlzzim/bui/font/iconfont.ttf') format('truetype'), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
            url('/wlzzim/bui/font/iconfont.svg#iconfont') format('svg'); /* iOS 4.1- */
        }
        .iconfont{
            font-family:"iconfont" !important;
            font-size:30px;font-style:normal;
            -webkit-font-smoothing: antialiased;
            color: white;
            -webkit-text-stroke-width: 0.2px;
            -moz-osx-font-smoothing: grayscale;}
        .icon{
            display: block;
            margin-top: 7px;
            margin-left: 3px;
        }
        .accept{
            width: 46px;
            height: 25px;
            background-color: #029ef4;
            color: white;
            border: 1px solid #009966;
        }
        .reject{
            width: 46px;
            height: 25px;
            background-color: tomato;
            margin-left: 2px;
            color: white;
            border: 1px solid #CCC;
        }
        .closeBtn{
            background: #CCC;
            margin-top: 3px;
        }
        .addFriendBtn{
            background: #00acf4;
            color: white;
            margin-top: 3px;
        }
    </style>
    <script src="/wlzzim/bui/js/zepto.js"></script>
    <script src="/wlzzim/bui/js/bui.js"></script>
</head>
<script>

    function searchToback() {
        bui.back();
    }
    function actionFriend(toUserid,action){
        if(action==0){
            var userid = androidFun.getUserId();
            var token = androidFun.getToken();
            $.post("http://39.108.97.134:8886/wlzz/getInfo/useridGetForInfo",{token:token,userid:toUserid},function(data) {
                var obj = eval('(' + data + ')');
                if(obj.code==0){
                    androidFun.newPage("与"+obj.data.nickName+"聊天","http://39.108.97.134:8801/wlzzim/index/chat?toUserid="+toUserid+"&userId="+userid);
                }else{
                    androidFun.showAndroidToast("该用户异常无法与他聊天");
                }
            });
        }
        else if(action==1){
            var token = androidFun.getToken();
            $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/applyForFriends",{token:token,toUserid:toUserid},function(data) {
                console.log(data);
                var obj = eval('(' + data + ')');
                if(obj.code==0){
                    layer.open({
                        content: '发出申请成功，等待对方处理'
                        ,skin: 'msg'
                        ,time: 2 //2秒后自动关闭
                    });
                }else{
                    layer.open({
                        content: obj.msg
                        ,skin: 'msg'
                        ,time: 2 //2秒后自动关闭
                    });
                }
            });
        }else{
            layer.closeAll();
        }

    }

    function searchFriends() {

        layer.open({
            type: 2,
            shade: false //不显示遮罩
        });
        var token = androidFun.getToken();
        var phone = document.getElementById("phone").value;
        $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/selectFriendsForPhone",{token:token,phone:phone},function(data) {
            console.log(data);
            var obj = eval('(' + data + ')');
            if(obj.code==0){
                console.log(obj);
                layer.closeAll();
                //自定义标题风格
                // layer.open({
                //     title: [
                //         '已为您搜到了TA',
                //         'background-color: #00acf4; color:#fff;'
                //     ]
                //     ,content:  '<main>'+
                //     '        <ul class="bui-list personal-info">'+
                //     '            <li class="bui-btn bui-box">'+
                //     '                <div class="thumbnail"><img src="'+getHeadImg(obj.data.imgUrl)+'" alt="" style="width: 60px;height: 60px"></div>'+
                //     '                <div class="span1">'+
                //     '                    <h3 class="item-title">'+obj.data.nickName+'</h3>'+
                //     '                </div>'+
                //     '            </li>'+
                //     '            <li class="bui-btn bui-box">'+
                //     '                <div class="span1">'+
                //     '                    <h5 class="item-title">学校：'+obj.data.schoolName+'<br>性别：'+obj.data.sex+'</h5>'+
                //     '                </div>'+
                //     '            </li>'+
                //     '        </ul>'+
                //     '            <div class="round primary" onclick="actionFriend(\''+obj.data.userId+'\',0)" style="height: 30px;padding-top: 4px;">去聊天</div>'+
                //     '            <div class="addFriendBtn" onclick="actionFriend(\''+obj.data.userId+'\',1)" style="height: 30px;padding-top: 4px;">加好友</div>'+
                //     '            <div class="closeBtn" onclick="actionFriend(\''+obj.data.userId+'\',2)" style="height: 30px;padding-top: 4px;">关闭</div>'+
                //     '    </main>',
                // });
                androidFun.gotoUserInfoPage(obj.data.userId);

            }else{
                layer.closeAll();
                layer.open({
                    content: obj.msg
                    ,skin: 'msg'
                    ,time: 2 //2秒后自动关闭
                });

            }
        });
    }
</script>
<body>
<div class="bui-page">
    <#--<header class="bui-bar">-->
        <#--<div class="bui-bar-left" onclick="searchToback()"><i class="iconfont icon">&#xe650;</i></div>-->
        <#--<div class="bui-bar-main">搜索或添加好友</div>-->
        <#--<div class="bui-bar-right"></div>-->
    <#--</header>-->
    <main>
        <!-- 搜索条控件结构 -->
        <div id="searchbar" class="bui-searchbar bui-box">
            <div class="span1">
                <div class="bui-input">
                    <i class="icon-search"></i>
                    <input type="search" value="" placeholder="请输入手机号码" id="phone"/>
                </div>
            </div>
            <div class="btn-search" onclick="searchFriends()">搜索</div>
        </div>
        <!-- 列表控件结构 -->
        <div id="scrollSearch" class="bui-scroll">
            <div class="bui-scroll-head"></div>
            <div class="bui-scroll-main">
                <ul class="bui-list">
                </ul>
            </div>
            <div id = "applyList"></div>
        </div>
    </main>
</div>
<script>
    function acceptFriend(id){
        var token = androidFun.getToken();
        layer.open({
            content: '您确定加对方为好友吗？'
            ,btn: ['确定', '取消']
            ,yes: function(index){
                $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/agreeForFriend",{token:token,_id:id},function(data) {
                    var obj = eval('(' + data + ')');
                    if(obj.code==0){
                        document.getElementById("friendType_"+id).innerHTML =  '<span style="color: #CCC">已接受</span>';
                    }else{
                        alert(obj.msg);
                    }
                });
                layer.close(index);
            }
        });
    }
    function rejectFriend(id){
        var token = androidFun.getToken();
        layer.open({
            content: '您确定拒绝对方为好友？'
            ,btn: ['残忍拒绝他', '取消']
            ,yes: function(index){
                $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/rejectForFriend",{token:token,_id:id},function(data) {
                    var obj = eval('(' + data + ')');
                    if(obj.code==0){
                        document.getElementById("friendType_"+id).innerHTML =  '<span style="color: #CCC">已拒绝</span>';
                    }else{
                        alert(obj.msg);
                    }
                });
                layer.close(index);
            }
        });
    }
    function friendType(status,id){
        if(status==0){
            return '<button class="accept" onclick="acceptFriend(\''+id+'\')">接受</button>' +
                    '<button class="reject" onclick="rejectFriend(\''+id+'\')">拒绝</button>'
        }else if(status==1){
            return '<span style="color: #CCC">已拒绝</span>';
        }else if(status==2){
            return '<span style="color: #CCC">已接受</span>';
        }
    }
    function getHeadImg(imgUrl){

        if(imgUrl=="/wlzz/images/logo.jpg"){
           return '${domainName}'+imgUrl;
        }else{
            return imgUrl;
        }
    }
    function getFriends(){
        $(function(){
            var token = androidFun.getToken();
            $.post("http://39.108.97.134:8886/wlzz/wlzzFriends/getApplyedList",{token:token},function(data){
                console.log(data);
                var obj = eval('(' + data + ')');
                if(obj.code==0){
                    var dataList = obj.data;
                    var allContent = "";
                    for(var i=0;i<dataList.length;i++){
                        var content = ' <li id="list_'+dataList[i].user.nickName+'">'+
                                '                      <div class="bui-btn bui-box listToChat">'+
                                '                        <img src="'+getHeadImg(dataList[i].user.imgUrl)+'" style="width: 45px;height: 45px">'+
                                '                                        &nbsp;&nbsp;&nbsp;<div class="span2">'+dataList[i].user.nickName+'<br></div>'+
                                '<div id="friendType_'+dataList[i].userWithFriends._id+'">'+friendType(dataList[i].userWithFriends.flag,dataList[i].userWithFriends._id)+'</div>'+
                                '                                    </div>'+
                                '                                </li>';
                        allContent = allContent + content;
                    }
                    document.getElementById("applyList").innerHTML = allContent;
                }else{
                    alert(obj.msg);
                }
            });
        });
    }
    getFriends();
</script>
</body>
</html>