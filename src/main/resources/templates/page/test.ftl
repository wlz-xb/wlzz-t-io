<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title>Personal3</title>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <script src="/wlzzim/miniui/jquery.min.js"></script>
    <link rel="stylesheet" href="/wlzzim/miniui/miniui/themes/default/miniui.css" />
    <script src="/wlzzim/miniui/miniui/miniui.js"></script>


</head>
<body>
<input id="helloBtn" class="mini-button" text="Hello" onclick="onHelloClick"/>
<script type="text/javascript">
    function onHelloClick(e) {
        var button = e.sender;
        mini.alert("Hello MiniUI!");
    }
</script>

</body>
</html>
